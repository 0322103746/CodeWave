import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import Animated from 'react-native-reanimated';

const QuestionsAndAnswers = () => {
  const [expanded, setExpanded] = useState(new Array(3).fill(false));

  const toggleQuestion = (index) => {
    const newExpanded = [...expanded];
    newExpanded[index] = !newExpanded[index];
    setExpanded(newExpanded);
  };

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Image source={require('../assets/M.jpg')} style={styles.titleImage} />
        <Text style={styles.title}>Preguntas Frecuentes</Text>
      </View>
      <ScrollView style={styles.scrollView}>
        <View style={styles.questionsContainer}>
          {questions.map((question, index) => (
            <TouchableOpacity
              key={index}
              style={styles.questionContainer}
              onPress={() => toggleQuestion(index)}
            >
              <View style={styles.questionTextContainer}>
                <Text style={styles.questionText}>{question.questionText}</Text>
              </View>
              {expanded[index] && (
                <Animated.View
                  style={[
                    styles.answerContainer,
                    {
                      height: 'auto',
                    },
                  ]}
                >
                  <Text style={styles.answerText}>{question.answerText}</Text>
                </Animated.View>
              )}
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
      <Text style={styles.crText}>© 2024, CodeWave</Text>
    </View>
  );
};

const questions = [
  {
    questionText: '¿Cómo puedo reservar un locker en la universidad?',
    answerText: ' + Puedes reservar un locker en nuestra aplicación móvil ingresando a la sección de lockers, seleccionando la ubicación deseada y siguiendo los pasos para completar la reserva. ',
  },
  {
    questionText: '¿Cuál es el costo de reservar un locker a través de la aplicación?',
    answerText: ' +  El costo de reservar un locker varía según la duración de la reserva y la disponibilidad en cada ubicación. Puedes encontrar esta información detallada dentro de la aplicación antes de confirmar tu reserva.',
  },
  {
    questionText: '¿Cómo puedo cancelar una reserva de locker?',
    answerText: '+ Para cancelar una reserva de locker, simplemente ingresa a la aplicación, dirígete a la sección de lockers, encuentra tu reserva activa y selecciona la opción para cancelar. Asegúrate de revisar nuestras políticas de cancelación para posibles cargos.',
  },
  {
    questionText: '¿Puedo cambiar la ubicación de mi locker reservado?',
    answerText: '+ Sí, puedes cambiar la ubicación de tu locker reservado siempre y cuando haya disponibilidad en la ubicación deseada. Para hacerlo, ponte en contacto con nuestro equipo de soporte a través de la aplicación para solicitar el cambio.',
  },
  {
    questionText: '¿Qué debo hacer si tengo problemas con mi locker o si olvidé mi código de acceso?',
    answerText: 'Si encuentras algún problema con tu locker o si olvidaste tu código de acceso, comunícate con nuestro equipo de soporte técnico a través de la aplicación. Estaremos encantados de ayudarte a resolver cualquier inconveniente y brindarte asistencia inmediata.'
  }
];

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 25,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  titleImage: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  scrollView: {
    flex: 1,
  },
  questionsContainer: {
    marginTop: 30,
  },
  questionContainer: {
    marginTop: 10,
  },
  questionTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  questionText: {
    fontSize: 18,
    lineHeight: 30,
    marginTop: 10,
    fontWeight: 'bold'
  },
  crText: {
    bottom: -15,
    fontSize: 12,
    color: '#E3DDD3',
    textAlign: 'center',
  },
});

export default QuestionsAndAnswers;
