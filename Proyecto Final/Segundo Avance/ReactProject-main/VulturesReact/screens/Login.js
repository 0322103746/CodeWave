
import axios from 'axios';
import { BASE_URL } from '../Config';

import { useState } from "react";
import { FontAwesome } from '@expo/vector-icons';
import { useUser } from '../components/UserProvider';
import { useNavigation } from '@react-navigation/native';
import { View, Image, Text, TextInput, Pressable, Alert, StyleSheet } from "react-native";

// -------------------------------------------------------------------------- //

import Logo from '../assets/adaptive-icon.png';

// Paleta de colores:
const azulOpaco = '#3B526A';
const negroOpaco = '#1C1C1E';
const blancoOpaco = '#E3DDD3';


const LoginScreen = () => {
    const { updateUser } = useUser();
    const navigation = useNavigation();

    const [ password, setPassword ] = useState('');
    const [ matricula, setMatricula ] = useState('');

    // ---------------------------------------------------------------------- //

    const onSignInPressed = async () => {
        try {
            const response = await fetch(`${BASE_URL}/v1/login/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    Matricula: matricula,
                    Clave: password,
                }),
            });

            if (!response.ok) {
                throw new Error('Error al iniciar sesión.');
            }

            const data = await response.json();
            updateUser(data);
            navigation.navigate('Profile');
        } catch (error) {
            console.log(error.message);
            Alert.alert("Error", `Error al iniciar sesión: ${error.message}`);
        }
    };


    const onForgotPasswordPressed = () => {
        console.warn("Ha olvidado su contraseña.");
    };

    // ---------------------------------------------------------------------- //

    return (
        <View style={styles.container}>
            <Image source={Logo} style={styles.logo} resizeMode="contain" />

            <View style={styles.inputContainer}>

                {/* Usuario: */}
                <View style={styles.inputView}>
                    <FontAwesome name="user" size={16} color={azulOpaco} style={styles.icon} />

                    <TextInput value={ matricula } onChangeText={ text => setMatricula(text) } placeholder="Matrícula"
                        placeholderTextColor="gray" style={styles.input} />
                </View>

                {/* Contraseña: */}
                <View style={styles.inputView}>
                    <FontAwesome name="lock" size={16} color={azulOpaco} style={styles.icon}/>

                    <TextInput value={ password } onChangeText={ text => setPassword(text) } placeholder="Contraseña"
                        secureTextEntry={ true } placeholderTextColor="gray" style={styles.input} />
                </View>

                <Pressable style={[ styles.button, { backgroundColor: azulOpaco, marginTop: 20 } ]} onPress={onSignInPressed}>
                    <Text style={styles.textFirst}>INGRESAR</Text>
                </Pressable>

                <Pressable style={styles.button} onPress={onForgotPasswordPressed}>
                    <Text style={styles.textSecond}>¿Ha olvidado su contraseña?</Text>
                </Pressable>

            </View>

            <Text style={styles.crText}>© CodeWave, 2024</Text>
        </View>
    );
};

export default LoginScreen;

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems : 'center',
        justifyContent: 'center',
        backgroundColor: negroOpaco,
    },

    logo: {
        width: '70%',
        maxWidth: 240,
        maxHeight: 240,
        marginBottom: 30,
    },

    inputContainer: {
        width: 320,
    },

    inputView: {
        width: '100%',
        marginVertical: 8,
        paddingVertical: 6,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 14,
        borderBottomWidth: 1,
        borderBottomColor: azulOpaco,
    },

    input: {
        flex: 1,
        color: blancoOpaco,
    },

    icon: {
        marginRight: 10,
    },

    button: {
        padding: 12,
        width: '100%',
        marginVertical: 5,
        alignItems: 'center',
    },

    textFirst: {
        fontSize: 15,
        color: blancoOpaco,
        fontWeight: 'bold',
    },

    textSecond: {
        fontSize: 14,
        color: blancoOpaco,
    },

    crText: {
        bottom: 14,
        fontSize: 12,
        color: blancoOpaco,
        position: 'absolute',
    },

});

// -------------------------------------------------------------------------- //