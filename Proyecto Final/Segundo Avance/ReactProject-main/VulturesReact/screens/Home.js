
import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons'; // Importa FontAwesome5

// -------------------------------------------------------------------------- //

// Paleta de colores:
const azulOscuro = '#02151E';

import Logo from '../assets/adaptive-icon.png';


const Home = () => {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.section}>
                <View style={styles.textContainer}>
                    <Text style={styles.heading}>Bienvenido a Lockit</Text>
                    <Text style={styles.paragraph}></Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={ Logo } style={styles.image} />
                </View>
            </View>

            <View style={styles.section}>
                <View style={styles.textContainer}>
                    <Text style={styles.heading}>¿Cuál es el propósito de Lockit?</Text>
                    <Text style={styles.paragraph}>
                        Esta aplicación ofrece una manera cómoda de gestionar la renta de casilleros, lo que garantiza una experiencia más segura para los usuarios. Además de brindar seguridad, Lockit proporciona una experiencia personalizada y divertida que fomenta la interacción del usuario con la aplicación. Desde la personalización de perfiles hasta la elección del casillero preferido, Lockit invita a los usuarios a participar activamente y a disfrutar de todas las funcionalidades que ofrece, creando así un entorno interactivo y atractivo.
                    </Text>
                </View>

            </View>

            {/* Información sobre los cursos */}
            <View style={styles.section}>
                <Text style={styles.heading}>Acciones permitidas en Lockit</Text>
                <View style={styles.courseContainer}>
                    <View style={styles.course}>
                        <FontAwesome5 name="user-edit" size={24} color="white"/>
                        <Text style={styles.courseTitle}>Personalización de perfil</Text>
                        <Text style={styles.courseDuration}>Te permite cambiar la imagen de fondo de tu perfil y la foto de perfil.</Text>
                    </View>
                    <View style={styles.course}>
                        <FontAwesome5 name="lock-open" size={24} color="white" />
                        <Text style={styles.courseTitle}>Personalización de casillero</Text>
                        <Text style={styles.courseDuration}>Te permite cambiar el aspecto de tu casillero.</Text>
                    </View>
                    <View style={styles.course}>
                        <FontAwesome5 name="wallet" size={24} color="white" />
                        <Text style={styles.courseTitle}>Renta de casillero</Text>
                        <Text style={styles.courseDuration}>Te permitirá rentar el casillero de tu preferencia vía PayPal.</Text>
                    </View>
                    <View style={styles.course}>
                        <FontAwesome5 name="headset" size={24} color="white" />
                        <Text style={styles.courseTitle}>Soporte</Text>
                        <Text style={styles.courseDuration}>Te permite comunicarte con los desarrolladores por cualquier problema</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1c1c1e',
    },
    section: {
        paddingHorizontal: 20,
        paddingBottom: 30,
        marginBottom: 20,
    },
    textContainer: {
        marginBottom: 20,
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        marginTop: 50,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center', // Centrar el texto
    },
    paragraph: {
        color: '#fff',
        fontSize: 16,
        textAlign: 'center', // Centrar el texto
    },
    imageContainer: {
        alignItems: 'center',
        marginBottom: 20,
    },
    image: {
        width: 200,
        height: 200, // Ajusta la altura de la imagen
        borderRadius: 40,
    },
    gifImage: {
        width: 350,
        height: 200,
        borderRadius: 10,
    },
    courseContainer: {
        flexWrap: 'wrap', // Añade envoltura
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10, // Agrega margen superior
    },
    course: {
        backgroundColor: azulOscuro,
        borderRadius: 10,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        width: '48%', // Ajusta el ancho del curso
        marginBottom: 20, // Añade margen inferior para separación
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    courseImage: {        width: 100,
        height: 100, // Ajusta la altura de la imagen
        marginBottom: 10,
        borderRadius: 10,
    },
    courseTitle: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center', // Centrar el texto
        marginBottom: 10,
    },
    courseDuration: {
        color: '#fff',
        fontSize: 14,
        textAlign: 'center', // Centrar el texto
    },
});

export default Home;