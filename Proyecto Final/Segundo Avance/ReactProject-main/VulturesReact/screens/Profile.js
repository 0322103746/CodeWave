
import { ScrollView, View, Image, ImageBackground, Text, TouchableOpacity, StyleSheet } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { useUser } from "../components/UserProvider";
import { Entypo } from '@expo/vector-icons';

// -------------------------------------------------------------------------- //

// Paleta de colores:
const azulOpaco = '#3B526A';
const negroOpaco = '#1C1C1E';
const blancoOpaco = '#E3DDD3';

// -------------------------------------------------------------------------- //

const Profile = () => {
    const { user } = useUser();
    const navigation = useNavigation();

    // Base URL del servidor.
    const baseUrl = 'http://localhost:8000';

    const defaultImage = require('../assets/UserImage.jpg');
    const defaultBackground = require('../assets/ImageBG.jpg');

    const profileImageUrl = user?.FotoPerfil ? `${baseUrl}${user.FotoPerfil}` : defaultImage;
    const coverImageUrl = user?.FotoPortada ? `${baseUrl}${user.FotoPortada}` : defaultBackground;

    const Nivel = () => {
        let tipoUsuario = 'INDEFINIDO';

        if (user?.Genero) {
            const Femina = user.Genero === 2;

            if (user?.Alumno) tipoUsuario = Femina ? 'Alumna' : 'Alumno';
            if (user?.Docente) tipoUsuario = Femina ? 'Docente' : 'Docente';
            if (user?.Administrativo) tipoUsuario = Femina ? 'Administrativa' : 'Administrativo';
        }

        return tipoUsuario;
    };

    // ---------------------------------------------------------------------- //

    return (
        <ImageBackground source={{ uri: coverImageUrl }} resizeMode='cover' style={styles.scrollView}>
            <View style={styles.darkOverlay}>

                <ScrollView style={styles.scrollView}>
                    <View style={styles.profileHeader}>
                        <Image source={{ uri: profileImageUrl }} style={styles.profileImage} />

                        <View style={styles.profileInfo}>
                            <Text style={styles.name}>{user?.Nombre} {user?.ApPaterno}</Text>

                            <View style={styles.levelContainer}>
                                <Text style={styles.level}>{Nivel()}</Text>
                            </View>

                        </View>
                    </View>

                    <View style={styles.section}>

                        <View style={styles.aboutMe}>
                            <Entypo name="info" size={16} color={blancoOpaco} style={styles.icon} />
                            <Text style={styles.titleAbout}>Sobre mí</Text>
                        </View>

                        <Text style={styles.aboutContent}>{user?.Biografia}</Text>
                    </View>

                    <TouchableOpacity onPress={() => navigation.navigate('ProfileDetails')} style={styles.buttonDetail}>
                        <Text style={styles.buttonText}>Detalles personales</Text>
                    </TouchableOpacity>
                </ScrollView>

            </View>
        </ImageBackground>
    );
};

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    scrollView: {
        flex: 1,
        width: '100%',
        height: '100%',
    },

    darkOverlay: {
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        backgroundColor: 'rgba(28, 28, 30, 0.8)',
    },

    profileHeader: {
        width: 440,
        marginTop: 50,
        flexDirection: 'row',
        alignItems: 'flex-end',
        alignSelf: 'center',
    },

    profileImage: {
        width: 140,
        height: 140,
        borderWidth: 2,
        borderColor: blancoOpaco,
        borderRadius: 70,
    },

    profileInfo: {
        flex: 1,
        marginLeft: 20,
    },

    name: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        color: blancoOpaco,
    },

    levelContainer: {
        backgroundColor: azulOpaco,
        paddingHorizontal: 12,
        paddingVertical: 6,
        marginTop: 14,
        borderRadius: 30,
    },

    level: {
        fontSize: 18,
        color: blancoOpaco,
        textAlign: 'center',
    },

    section: {
        width: 440,
        marginTop: 50,
        marginBottom: 20,
        alignSelf: 'center',

    },

    aboutMe: {
        paddingVertical: 8,
        alignItems: 'center',
        flexDirection: 'row',
    },

    icon: {
        marginRight: 10,
    },

    titleAbout: {
        fontSize: 18,
        color: blancoOpaco,
        fontWeight: 'bold',
        textAlignVertical: 'center',
    },

    aboutContent: {
        fontSize: 16,
        marginTop: 8,
        color: blancoOpaco,
        textAlign: 'justify',
    },

    buttonDetail: {
        width: 440,
        borderRadius: 30,
        paddingVertical: 6,
        alignSelf: 'center',
        backgroundColor: azulOpaco,
    },

    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        color: blancoOpaco,
    }

});

export default Profile;