import React from 'react';
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons';

import Logo from '../assets/adaptive-icon.png';

const About = () => {
  return (
    <ScrollView style={styles.container}>
      <Image style={styles.image} source={Logo} />
      <Text style={styles.title}>CodeWave</Text>
      <Text style={styles.text}>
        {`¡Hola! Somos CodeWave, una empresa de desarrollo de aplicaciones móviles especializada en React Native.`}
      </Text>
      <Text style={styles.text}>
        {`Nuestro equipo está formado por desarrolladores experimentados que han trabajado en una amplia variedad de proyectos, desde pequeñas startups hasta grandes empresas.`}
      </Text>
      <Text style={styles.text}>
        {`Nos enorgullecemos de ofrecer la mejor experiencia de usuario posible mediante el uso de las tecnologías y prácticas más recientes.`}
      </Text>
      <Text style={styles.text}>
        {`Si tienes alguna pregunta o deseas obtener más información sobre nuestros servicios, no dudes en ponerte en contacto con nosotros. ¡Estamos deseando escuchar de ti!`}
      </Text>
      <View style={styles.socialIcons}>
        <TouchableOpacity style={styles.iconButton}>
          <Feather name="facebook" size={24} color="#3b5998"/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton}>
          <Feather name="twitter" size={24} color="#1da1f2" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton}>
          <Feather name="instagram" size={24} color="#e4405f" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconButton}>
          <Feather name='youtube' size={24} color="#1C1C1E" />
        </TouchableOpacity>
      </View>
      <Text style={styles.crText}>© 2024, CodeWave</Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 10,
  },
  image: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20,
    borderRadius: 50,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 30,
  },
  text: {
    fontSize: 16,
    lineHeight: 24,
    marginTop: 20,
    textAlign: 'center',
  },
  socialIcons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  iconButton: {
    backgroundColor: 'transparent',
    borderColor: '#1C1C1E',
    borderWidth: 1,
    borderRadius: 24,
    padding: 10,
    elevation: 0,
  },
  crText: {
    bottom: 0,
    fontSize: 12,
    color: '#E3DDD3',
    textAlign: 'center',
},
});

export default About;