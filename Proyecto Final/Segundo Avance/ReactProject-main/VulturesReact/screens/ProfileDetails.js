
import React from 'react';
import { useUser } from "../components/UserProvider";
import { useNavigation } from '@react-navigation/native';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native';

import { FontAwesome } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

// -------------------------------------------------------------------------- //

// Paleta de colores:
const azulOpaco = '#3B526A';
const negroOpaco = '#1C1C1E';
const blancoOpaco = '#E3DDD3';


const ProfileDetails = () => {
    const { user } = useUser();
    const navigation = useNavigation();

    // Base URL del servidor.
    const baseUrl = 'http://localhost:8000';

    const defaultBackground = require('../assets/ImageBG.jpg');
    const coverImageUrl = user?.FotoPortada ? `${baseUrl}${user.FotoPortada}` : defaultBackground;

    // ---------------------------------------------------------------------- //

    // Formatea los datos DateTime.
    const formatDateTime = (dateTimeString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(dateTimeString).toLocaleDateString('es-ES', options);
    };

    // Formate los datos Date.
    const formatDateOnly = (dateString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' };
        // Asumir UTC para evitar desfase por zona horaria
        const date = new Date(dateString + 'T00:00:00Z');
        return date.toLocaleDateString('es-ES', options);
    };

    return (
        <ImageBackground source={{ uri: coverImageUrl }} resizeMode='cover' style={styles.scrollView}>
            <View style={styles.darkOverlay}>

                <View style={styles.container}>
                    <View style={styles.infoDetail}>
                        <MaterialCommunityIcons name="email" size={16} color={blancoOpaco} />
                        <Text style={styles.infoTitle}>Correo</Text>
                        <Text style={styles.detailText}>: {user?.Email}</Text>
                    </View>

                    <View style={styles.infoDetail}>
                        <FontAwesome name="phone" size={16} color={blancoOpaco} />
                        <Text style={styles.infoTitle}>Número</Text>
                        <Text style={styles.detailText}>: {user?.NumTel}</Text>
                    </View>

                    <View style={styles.infoDetail}>
                        <FontAwesome name="birthday-cake" size={16} color={blancoOpaco} />
                        <Text style={styles.infoTitle}>Cumpleaños</Text>
                        <Text style={styles.detailText}>: {formatDateOnly(user?.Natalicio)}</Text>
                    </View>

                    <View style={styles.infoDetail}>
                        <MaterialIcons name="access-time-filled" size={16} color={blancoOpaco} />
                        <Text style={styles.infoTitle}>Registro</Text>
                        <Text style={styles.detailText}>: {formatDateTime(user?.FechaRegistro)}</Text>
                    </View>

                    {/* <View style={styles.infoDetail}>
                        <MaterialCommunityIcons name="list-status" size={16} color={blancoOpaco} />
                        <Text style={styles.infoTitle}>Estado</Text>
                        <Text style={styles.detailText}>: {user?.EstadoNombre}</Text>
                    </View> */}

                    <TouchableOpacity onPress={() => { navigation.navigate('Profile') }} style={styles.editButton}>
                        <Text style={styles.editButtonText}>Editar Perfil</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </ImageBackground>
    );
};

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    scrollView: {
        flex: 1,
        width: '100%',
        height: '100%',
    },

    darkOverlay: {
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        backgroundColor: 'rgba(28, 28, 30, 0.8)',
    },

    container: {
        width: 440,
        marginTop: 50,
        marginBottom: 20,
        alignSelf: 'center',
    },

    infoDetail: {
        paddingVertical: 8,
        alignItems: 'center',
        flexDirection: 'row',
    },

    icon: {
        marginRight: 10,
    },

    infoTitle: {
        fontSize: 16,
        marginLeft: 6,
        fontWeight: 'bold',
        color: blancoOpaco,
        textAlignVertical: 'center',
    },

    detailText: {
        fontSize: 16,
        color: blancoOpaco,
    },

    editButton: {
        width: 440,
        marginTop: 30,
        borderRadius: 30,
        paddingVertical: 8,
        alignSelf: 'center',
        backgroundColor: azulOpaco,
    },

    editButtonText: {
        fontSize: 16,
        textAlign: 'center',
        color: blancoOpaco,
    },

});

export default ProfileDetails;
