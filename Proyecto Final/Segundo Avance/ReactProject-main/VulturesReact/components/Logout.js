
import React from 'react';

import { FontAwesome } from '@expo/vector-icons';
import { useUser } from '../components/UserProvider';
import { useNavigation } from '@react-navigation/native';
import { View, Pressable, Text, StyleSheet } from 'react-native';


// -------------------------------------------------------------------------- //

// Paleta de colores:
const azulOpaco = '#3B526A';
const negroOpaco = '#1C1C1E';
const blancoOpaco = '#E3DDD3';


const LogoutButton = () => {
    const { logout } = useUser();
    const navigation = useNavigation();

    const handleLogout = () => {
        logout();
        navigation.navigate('Login');
    };

    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Pressable style={styles.outButton} onPress={handleLogout}>
                <FontAwesome name="sign-out" size={16} color={blancoOpaco} style={styles.outIcon} />
                <Text style={styles.outText}>Salir</Text>
            </Pressable>
        </View>
    );
};

export default LogoutButton;


const styles = StyleSheet.create({

    outButton: {
        marginVertical: 6,
        padding: 12,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
    },

    outIcon: {
        marginRight: 12,
    },

    outText: {
        fontSize: 16,
        color: blancoOpaco,
    },

});