
import { BASE_URL } from '../Config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { createContext, useContext, useEffect, useState } from 'react';

// -------------------------------------------------------------------------- //

const UserContext = createContext();

export const UserProvider = ({ children }) => {
    const [ user, setUser ] = useState(null);

    const updateUser = async (userData) => {

        try {

            // ------------------------------------------------------------------ //

            if (userData.Genero) {

                try {
                    const responseGeneros = await fetch(`${BASE_URL}/v1/generos/list/`, );
                    const Generos = await responseGeneros.json();
                    const userGenero = Generos.find(genero => genero.id === userData.Genero);
                    userData = {...userData, GeneroNombre: userGenero ? userGenero.Genero : 'Desconocido'};
                } catch (error) {
                    console.error("Ha ocurrido un pequeño fallo: ", error);
                }
            }

            if (userData.Estado) {

                try {
                    const responseEstados = await fetch(`${BASE_URL}/v1/estados/list/`, );
                    const Estados = await responseEstados.json();
                    const userEstado = Estados.find(estado => estado.id === userData.Estado);
                    userData = {...userData, EstadoNombre: userEstado ? userEstado.Estado : 'Desconocido'};

                } catch (error) {
                    console.error("Ha ocurrido un pequeño fallo: ", error);
                }
            }

            // ------------------------------------------------------------------ //

            // Actualización del estado y almacenamiento en AsyncStorage
            setUser(userData);
            await AsyncStorage.setItem('userData', JSON.stringify(userData));

        } catch (error) {
            console.error("Error al actualizar el usuario: ", error);
        }
    };

    // ---------------------------------------------------------------------- //

    const logout = async () => {
        setUser(null);
        await AsyncStorage.removeItem('userData');
    };


    useEffect(() => {
        const loadUser = async () => {
            const userDataString = await AsyncStorage.getItem('userData');
            if (userDataString) {
                const userData = JSON.parse(userDataString);
                setUser(userData);
            }
        };
        loadUser();
    }, []);


    return (
        <UserContext.Provider value={{ user, updateUser, logout }}>
            {children}
        </UserContext.Provider>
    );
};

export const useUser = () => useContext(UserContext);