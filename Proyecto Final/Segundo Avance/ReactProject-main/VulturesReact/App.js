
// -------------------------------------------------------------------------- //

import NavMenu from './NavMenu';
import { UserProvider } from './components/UserProvider';

// -------------------------------------------------------------------------- //

export default function App() {

  return (
      <UserProvider>
        <NavMenu />
      </UserProvider>
  );

};

// -------------------------------------------------------------------------- //