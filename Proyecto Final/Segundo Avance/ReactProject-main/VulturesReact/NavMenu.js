
import 'react-native-gesture-handler';

import React, { createContext, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer';
import { View, ScrollView, SafeAreaView, Image, Text, StyleSheet } from 'react-native';

// -------------------------------------------------------------------------- //

// import UserImage from './assets/UserImage.jpg';
import { useUser } from './components/UserProvider'


import Home from './screens/Home';
import About from './screens/About';
import QuestionsAndAnswers from './screens/QuestionsAndAnswers'
import Lockers from './screens/Lockers';
import Login from './screens/Login';
import Profile from './screens/Profile';
import ProfileDetails from './screens/ProfileDetails';
import Settings from './screens/Settings';
import LogoutButton from './components/Logout';

const Drawer = createDrawerNavigator();

// Paleta de colores:
const azulOpaco = '#3B526A';
const negroOpaco = '#1C1C1E';
const blancoOpaco = '#E3DDD3';


const NavMenu = () => {
    const { user } = useUser();

    // Base URL del servidor.
    const baseUrl = 'http://localhost:8000';
    const defaultImage = require('./assets/UserImage.jpg');
    const profileImageUrl = user?.FotoPerfil ? `${baseUrl}${user.FotoPerfil}` : defaultImage;

    return (

        <NavigationContainer>
            <Drawer.Navigator

                drawerContent = { (props) => {

                    return (
                        <SafeAreaView style={{flex: 1}}>
                            <View style={styles.profileContainer}>
                                <Image source={{ uri: profileImageUrl }} style={styles.profilePicture} />
                                <Text style={styles.profileName}>{user?.Nombre}</Text>
                            </View>


                            <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false} >
                                <DrawerItemList {...props} />
                            </ScrollView>

                            <View style={{padding: 10}}>
                                <LogoutButton />
                            </View>
                        </SafeAreaView>
                    );
                }}

                // ---------------------------------------------------------- //

                screenOptions = {{

                    // ENCABEZADO: ------------------------------------------ //

                    headerShown: true,
                    headerTintColor: blancoOpaco,
                    headerStyle: { borderBottomWidth: 0, backgroundColor: azulOpaco },

                    // PANEL: ----------------------------------------------- //

                    drawerActiveBackgroundColor: blancoOpaco,
                    drawerActiveTintColor: azulOpaco,
                    drawerInactiveTintColor: blancoOpaco,
                    drawerStyle: styles.drawerStyle,
                    overlayColor: "rgba(28, 28, 30, .6)",
                    drawerLabelStyle: styles.drawerLabelStyle, }}>


                <Drawer.Screen name='Home' component={ Home } options={{
                    title : "Inicio" }} />

                <Drawer.Screen name='Profile' component={ Profile } options={{
                    title : "Mi perfil" }} />

                <Drawer.Screen name="ProfileDetails" component={ ProfileDetails } options={{
                    title : "Detalles personales", drawerItemStyle: { display: 'none' } }} />


                <Drawer.Screen name='Lockers' component={ Lockers } options={{
                    title : "Mi casillero" }} />

                <Drawer.Screen name='Settings' component={ Settings } options={{
                    title : "Configuración",  drawerItemStyle: { display: 'none' } }} />

                <Drawer.Screen name='About' component={ About } options={{
                    title : "Sobre nosotros" }} />

                <Drawer.Screen name='FAQ' component={ QuestionsAndAnswers } options={{
                    title : "Preguntas frecuentes" }} />


                <Drawer.Screen name='Login' component={ Login } options={{
                    title : "Iniciar sesión", drawerItemStyle: { display: 'none' } }} />

            </Drawer.Navigator>
        </NavigationContainer>

    );
};

export default NavMenu;

// -------------------------------------------------------------------------- //

const styles = StyleSheet.create({

    profileContainer: {
        paddingTop: 80,
        paddingBottom: 40,
        backgroundColor: azulOpaco,
    },

    profilePicture: {
        width: 200,
        height: 200,
        borderWidth: 3,
        marginBottom: 40,
        borderRadius: 200,
        alignSelf: 'center',
        borderColor: blancoOpaco,
    },

    profileName: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        color: blancoOpaco,
    },

    drawerLabelStyle: {
        fontSize: 15,
    },

    drawerStyle: {
        backgroundColor: azulOpaco,
    },

    icon: {
        marginLeft: 10,
    },

});

// -------------------------------------------------------------------------- //