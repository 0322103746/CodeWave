
# python manage.py shell < Datillos.py

import os
import django
from django.utils.dateparse import parse_datetime

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'DatabaseAPI.settings')
django.setup()

from core.models import *

# ---------------------------------------------------------------------------- #

Carreras.objects.create(Nombre='Tecnología Ambiental')
Carreras.objects.create(Nombre='Energías Renovables')
Carreras.objects.create(Nombre='Manufactura Aeronáutica')
Carreras.objects.create(Nombre='Mecatrónica')
Carreras.objects.create(Nombre='Entornos Virtuales & Negocios Digitales')
Carreras.objects.create(Nombre='Desarrollo & Gestión de Software')
Carreras.objects.create(Nombre='Redes Inteligentes & Ciberseguridad')
Carreras.objects.create(Nombre='Procesos & Operaciones Industriales')
Carreras.objects.create(Nombre='Electromecánica Industrial')
Carreras.objects.create(Nombre='Logística Comercial Global')
Carreras.objects.create(Nombre='Contaduría')
Carreras.objects.create(Nombre='Gestión de Redes Logísticas')
Carreras.objects.create(Nombre='Innovación de Negocios & Mercadotecnia')


Grupos.objects.create(Grado='2', Grupo='A')
Grupos.objects.create(Grado='2', Grupo='B')
Grupos.objects.create(Grado='2', Grupo='C')
Grupos.objects.create(Grado='4', Grupo='A')
Grupos.objects.create(Grado='4', Grupo='B')
Grupos.objects.create(Grado='4', Grupo='C')
Grupos.objects.create(Grado='6', Grupo='A')
Grupos.objects.create(Grado='6', Grupo='B')
Grupos.objects.create(Grado='6', Grupo='C')


Grupillo = 6
Carrerilla = 6

dataAlumnos = [
    Alumnos(Grupo_id=Grupillo, Carrera_id=Carrerilla),
    Alumnos(Grupo_id=Grupillo, Carrera_id=Carrerilla),
    Alumnos(Grupo_id=Grupillo, Carrera_id=Carrerilla),
    Alumnos(Grupo_id=Grupillo, Carrera_id=Carrerilla),
    Alumnos(Grupo_id=Grupillo, Carrera_id=Carrerilla)
]

Alumnos.objects.bulk_create(dataAlumnos)


Puestos.objects.create(Nombre='Director')
Puestos.objects.create(Nombre='Coordinador Académico')
Puestos.objects.create(Nombre='Jefe de Departamento')
Puestos.objects.create(Nombre='Secretario Administrativo')
Puestos.objects.create(Nombre='Encargado de Recursos Humanos')
Puestos.objects.create(Nombre='Contador de Finanzas')
Puestos.objects.create(Nombre='Asistente Administrativo')
Puestos.objects.create(Nombre='Coordinador de Comunicación')
Puestos.objects.create(Nombre='Encargado de Admisiones')
Puestos.objects.create(Nombre='Coordinador de Actividades Estudiantiles')


Departamentos.objects.create(Nombre='Dirección')
Departamentos.objects.create(Nombre='Coordinación Académica')
Departamentos.objects.create(Nombre='Recursos Humanos')
Departamentos.objects.create(Nombre='Administración')
Departamentos.objects.create(Nombre='Finanzas')
Departamentos.objects.create(Nombre='Comunicación')
Departamentos.objects.create(Nombre='Admisiones')
Departamentos.objects.create(Nombre='Actividades Estudiantiles')
Departamentos.objects.create(Nombre='Servicios Administrativos')
Departamentos.objects.create(Nombre='Desarrollo Estudiantil')


dataPD = [
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (9, 9),
    (10, 10),
]

for puesto_id, depto_id in dataPD:
    puesto_instance = Puestos.objects.get(id=puesto_id)
    puesto_depto_instance = PuestoDepto.objects.create(Puesto=puesto_instance)
    departamento_instance = Departamentos.objects.get(id=depto_id)
    puesto_depto_instance.Departamento.add(departamento_instance)


dataAdmin = [
    (5, 5),
    (7, 7),
]

for puesto_id, departamento_id in dataAdmin:
    puesto = Puestos.objects.get(id=puesto_id)
    departamento = Departamentos.objects.get(id=departamento_id)
    Administrativos.objects.create(Puesto=puesto, Departamento=departamento)


dataDocentes = [
    3,
    5,
    6,
    12,
]

for carrera_id in dataDocentes:
    carrera = Carreras.objects.get(id=carrera_id)
    Docentes.objects.create(Carrera=carrera)



Generos.objects.create(Genero='Masculino')
Generos.objects.create(Genero='Femenino')


Estados.objects.create(Estado='Activo')
Estados.objects.create(Estado='Inactivo')


dataUsuarios = [
    ('Vultures.22', '0322103814', 'Ed', 'Rubio', None, 1, '2000-11-14', '(664) 264-4682', 'ed.cetacean@outlook.com', 1, None, None, 1),
    ('Vultures.24', '0322103824', 'Steve', 'Olmos', 'Labastida', 1, '1997-06-12', '(664) 268-4682', 'steve.olmos@outlook.com', 2, None, None, 1),
    ('Vultures.26', '0322103826', 'Jaime Isaac', 'López', 'Guerrero', 2, '2004-04-11', '(664) 244-4682', 'cyberhacks@outlook.com', 3, None, None, 1),
    ('Vultures.28', '0322103828', 'Hugo', 'Macareno', 'Rivera', 1, '2005-12-05', '(664) 204-4682', 'hugo.macareno@outlook.com', 4, None, None, 1),
    ('Vultures.30', '0322103830', 'Gabriel', 'GR', None, 1, '2002-02-11', '(664) 640-4682', 'gabo@outlook.com', 5, None, None, 1),

    ('Vultures.32', '0322103832', 'Taylor', 'Miller', 'Johnson', 2, '1990-02-16', '(664) 168-1610', 'taylor.miller@outlook.com', None, 1, None, 1),
    ('Vultures.34', '0322103834', 'Riley', 'Smith', 'Jones', 1, '1988-05-24', '(664) 944-5897', 'riley.smith@outlook.com', None, 2, None, 1),
    ('Vultures.36', '0322103836', 'Emi', 'Garcia', None, 2, '1994-04-14', '(664) 935-1688', 'emili@outlook.com', None, 3, None, 1),
    ('Vultures.38', '0322103838', 'Alex', 'Brown', None, 1, '1990-01-02', '(664) 402-2593', 'alex.brown@outlook.com', None, 4, None, 1),

    ('Vultures.40', '0322103840', 'Miller', 'Brown', None, 1, '1984-08-26', '(664) 126-9588', 'jamie.miller@outlook.com', None, None, 1, 1),
    ('Vultures.42', '0322103842', 'Riley', 'Johnson', 'Davis', 2, '1994-07-22', '(664) 193-6697', 'riley.johnson94@outlook.com', None, None, 2, 1),
]

for clave, matricula, nombre, ap_paterno, ap_materno, genero_id, natalicio, num_tel, email, alumno_id, docente_id, administrativo_id, estado_id in dataUsuarios:
    genero = Generos.objects.get(id=genero_id)
    estado = Estados.objects.get(id=estado_id)
    alumno = Alumnos.objects.get(id=alumno_id) if alumno_id else None
    docente = Docentes.objects.get(id=docente_id) if docente_id else None
    administrativo = Administrativos.objects.get(id=administrativo_id) if administrativo_id else None

    natalicio_dt = parse_datetime(natalicio)
    Usuarios.objects.create(
        Clave=clave,
        Matricula=matricula,
        Nombre=nombre,
        ApPaterno=ap_paterno,
        ApMaterno=ap_materno,
        Genero=genero,
        Natalicio=natalicio_dt,
        NumTel=num_tel,
        Email=email,
        Alumno=alumno,
        Docente=docente,
        Administrativo=administrativo,
        Estado=estado
    )


dataDocencias = [
    'Docencia 1',
    'Docencia 2',
    'Docencia 3',
    'Docencia 4',
    'Docencia 5',
]

for nombre in dataDocencias:
    Docencias.objects.create(Nombre=nombre)


dataUbicaciones = [
    ('1', 1),
    ('2', 1),
    ('1', 2),
    ('2', 2),
    ('1', 3),
    ('2', 3),
    ('1', 4),
    ('2', 4),
    ('1', 5),
    ('2', 5),
]

for piso, docencia_id in dataUbicaciones:
    docencia = Docencias.objects.get(id=docencia_id)
    Ubicaciones.objects.create(Piso=piso, Docencia=docencia)


dataTamanos = [
    ('Chico', '30x15x30', 80.00),
    ('Grande', '50x25x50', 120.00),
]

for descripcion, dimensiones, precio in dataTamanos:
    Tamanos.objects.create(Descripcion=descripcion, Dimensiones=dimensiones, Precio=precio)


dataDisponibilidad = [
    'Disponible',
    'Ocupado',
]

for descripcion in dataDisponibilidad:
    Disponibilidades.objects.create(Descripcion=descripcion)


dataCasilleros = [
    (1, 1, 1), (1, 1, 2), (1, 1, 2), (1, 1, 1), (1, 1, 1),
    (1, 1, 1), (1, 1, 1), (1, 1, 1), (1, 1, 2), (1, 1, 1),
    (1, 1, 1), (1, 1, 2), (1, 2, 1), (1, 2, 2), (1, 2, 2),
    (1, 2, 1), (1, 2, 1), (1, 2, 2), (1, 2, 1), (1, 2, 1),
    (1, 2, 2), (1, 2, 2), (1, 2, 1), (1, 2, 1), (1, 3, 1),
    (1, 3, 2), (1, 3, 2), (1, 3, 1), (1, 3, 1), (1, 3, 1),
    (1, 3, 1), (1, 3, 1), (1, 3, 2), (1, 3, 1), (1, 3, 1),
    (1, 3, 2), (1, 4, 1), (1, 4, 2), (1, 4, 2), (1, 4, 1),
    (1, 4, 1), (1, 4, 2), (1, 4, 1), (1, 4, 1), (1, 4, 2),
    (1, 4, 2), (1, 4, 1), (1, 4, 1), (1, 5, 1), (1, 5, 2),
    (1, 5, 2), (1, 5, 1), (1, 5, 1), (1, 5, 1), (1, 5, 1),
    (1, 5, 1), (1, 5, 2), (1, 5, 1), (1, 5, 1), (1, 5, 2),
    (1, 6, 1), (1, 6, 2), (1, 6, 2), (1, 6, 1), (1, 6, 1),
    (1, 6, 2), (1, 6, 1), (1, 6, 1), (1, 6, 2), (1, 6, 2),
    (1, 6, 1), (1, 6, 1), (1, 7, 1), (1, 7, 2), (1, 7, 2),
    (1, 7, 1), (1, 7, 1), (1, 7, 1), (1, 7, 1), (1, 7, 1),
    (1, 7, 2), (1, 7, 1), (1, 7, 1), (1, 7, 2), (1, 8, 1),
    (1, 8, 2), (1, 8, 2), (1, 8, 1), (1, 8, 1), (1, 8, 2),
    (1, 8, 1), (1, 8, 1), (1, 8, 2), (1, 8, 2), (1, 8, 1),
    (1, 8, 1), (1, 9, 1), (1, 9, 2), (1, 9, 2), (1, 9, 1),
    (1, 9, 1), (1, 9, 1), (1, 9, 1), (1, 9, 1), (1, 9, 2),
    (1, 9, 1), (1, 9, 1), (1, 9, 2), (1, 10, 1), (1, 10, 2),
    (1, 10, 2), (1, 10, 1), (1, 10, 1), (1, 10, 2), (1, 10, 1),
    (1, 10, 1), (1, 10, 2), (1, 10, 2), (1, 10, 1), (1, 10, 1),
]

objetos_casilleros = [
    Casilleros(Disponibilidad_id=disponibilidad, Ubicacion_id=ubicacion, Tamano_id=tamano)
    for disponibilidad, ubicacion, tamano in dataCasilleros
]

Casilleros.objects.bulk_create(objetos_casilleros)


dataPeriodos = [
    ('Enero - Abril', 1, 4),
    ('Mayo - Agosto', 5, 8),
    ('Septiembre - Diciembre', 9, 12),
]

for item in dataPeriodos:
    periodo = Periodos(Periodo=item[0], MesInicio=item[1], MesFInal=item[2])
    periodo.save()

# ---------------------------------------------------------------------------- #

print("Datos insertados con éxito.")