
from rest_framework import serializers
from core.models import *


class CarrerasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carreras
        fields = '__all__'


class GruposSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grupos
        fields = '__all__'


class AlumnosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alumnos
        fields = '__all__'


class PuestosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Puestos
        fields = '__all__'


class DepartamentosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departamentos
        fields = '__all__'


class PuestoDeptoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PuestoDepto
        fields = '__all__'


class AdministrativosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Administrativos
        fields = '__all__'


class DocentesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Docentes
        fields = '__all__'


class GenerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Generos
        fields = '__all__'


class EstadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estados
        fields = '__all__'


class UsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = '__all__'


class DocenciasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Docencias
        fields = '__all__'


class UbicacionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ubicaciones
        fields = '__all__'


class TamanosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tamanos
        fields = '__all__'


class DisponibilidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disponibilidades
        fields = '__all__'


class CasillerosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Casilleros
        fields = '__all__'


class PeriodosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Periodos
        fields = '__all__'


class RentasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rentas
        fields = '__all__'


class PagosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pagos
        fields = '__all__'

## -------------------------------------------------------------------------- //