
from django.contrib import admin
from . import models

admin.site.register(models.Carreras)
admin.site.register(models.Grupos)
admin.site.register(models.Alumnos)
admin.site.register(models.Puestos)
admin.site.register(models.Departamentos)
admin.site.register(models.PuestoDepto)
admin.site.register(models.Administrativos)
admin.site.register(models.Docentes)
admin.site.register(models.Generos)
admin.site.register(models.Estados)
admin.site.register(models.Usuarios)
admin.site.register(models.Docencias)
admin.site.register(models.Ubicaciones)
admin.site.register(models.Tamanos)
admin.site.register(models.Disponibilidades)
admin.site.register(models.Casilleros)
admin.site.register(models.Periodos)
admin.site.register(models.Rentas)
admin.site.register(models.Pagos)