
import datetime
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator
from django.utils.translation import gettext_lazy as _
from django.core.validators import FileExtensionValidator;


class Carreras(models.Model):
    Nombre = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.Nombre


class Grupos(models.Model):
    Grado = models.CharField(max_length=2, null=False)
    Grupo = models.CharField(max_length=2, null=False)

    def __str__(self):
        return f"{self.Grado}° {self.Grupo}"


class Alumnos(models.Model):
    Grupo = models.ForeignKey(Grupos, on_delete=models.CASCADE, null=False)
    Carrera = models.ForeignKey(Carreras, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.id)


class Puestos(models.Model):
    Nombre = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.Nombre


class Departamentos(models.Model):
    Nombre = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.Nombre


class PuestoDepto(models.Model):
    Puesto = models.ForeignKey(Puestos, on_delete=models.CASCADE)
    Departamento = models.ManyToManyField(Departamentos)

    def __str__(self):
        return "{} / {}".format(self.Departamento, ', '.join(self.Puesto.all().values_list('Puesto', flat=True)))


class Administrativos(models.Model):
    Puesto = models.ForeignKey(Puestos, on_delete=models.CASCADE, null=False)
    Departamento = models.ForeignKey(Departamentos, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.id)


class Docentes(models.Model):
    Carrera = models.ForeignKey(Carreras, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class Generos(models.Model):
    Genero = models.CharField(default="Masculino", max_length=32, null=False)

    def __str__(self):
        return self.Genero


class Estados(models.Model):
    Estado = models.CharField(max_length=32, null=False)

    def __str__(self):
        return self.Estado


def validacionEdad(value):
    FechaActual = datetime.date.today()
    DiferenciaEdad = FechaActual - datetime.timedelta(days=18*365.25)

    if isinstance(value, datetime.datetime):
        value = value.date()

    if value > DiferenciaEdad:
        raise ValidationError(_('Debe poseer un mínimo de 18 años para poder registrarse.'), params={'value': value})

class Usuarios(models.Model):
    # HuellaDigital = models.BinaryField()
    FotoPerfil = models.ImageField(upload_to='usuarios/perfil/', null=True, blank=True, validators=[FileExtensionValidator(['jpg', 'jpeg', 'png'])])
    FotoPortada = models.ImageField(upload_to='usuarios/portada/', null=True, blank=True, validators=[FileExtensionValidator(['jpg', 'jpeg', 'png'])])


    Clave = models.CharField(default="123456789", max_length=128, null=False)
    Matricula = models.CharField(max_length=10, null=False, unique=True)
    Biografia = models.CharField(max_length=512, null=True, default='No les mentiré... usé el Chat.')

    Nombre = models.CharField(default="Steve", max_length=128, null=False)
    ApPaterno = models.CharField(default="Olmos", max_length=64, null=False)
    ApMaterno = models.CharField(default="Labastida", max_length=64, null=True, blank=True)
    Genero = models.ForeignKey(Generos, on_delete=models.CASCADE)
    Natalicio = models.DateField(null=False, validators=[validacionEdad]) # Fecha de nacimiento.
    NumTel = models.CharField(default="(666) 666-9698", max_length=16)
    Email = models.CharField(max_length=128, null=False, unique=True)

    Alumno = models.ForeignKey(Alumnos, on_delete=models.CASCADE, null=True, blank=True)
    Docente = models.ForeignKey(Docentes, on_delete=models.CASCADE, null=True, blank=True)
    Administrativo = models.ForeignKey(Administrativos, on_delete=models.CASCADE, null=True, blank=True)

    FechaRegistro = models.DateTimeField(auto_now_add=True, auto_now=False)
    Estado = models.ForeignKey(Estados, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.Nombre} {self.ApPaterno}"


class Docencias(models.Model):
    Nombre = models.CharField(max_length=64, null=False)

    def __str__(self):
        return self.Nombre


class Ubicaciones(models.Model):
    Piso = models.CharField(max_length=1, null=False)
    Docencia = models.ForeignKey(Docencias, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return f"{self.Piso} - {self.Docencia}"


class Tamanos(models.Model):
    Descripcion = models.CharField(max_length=64, null=False)
    Dimensiones = models.CharField(max_length=16, null=False)
    Precio = models.FloatField(null=False)

    def __str__(self):
        return self.Descripcion


class Disponibilidades(models.Model):
    Descripcion = models.CharField(max_length=16, null=False)

    def __str__(self):
        return self.Descripcion


class Casilleros(models.Model):
    Disponibilidad = models.ForeignKey(Disponibilidades, on_delete=models.CASCADE, null=False)
    Ubicacion = models.ForeignKey(Ubicaciones, on_delete=models.CASCADE, null=False)
    Tamano = models.ForeignKey(Tamanos, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.id)


class Periodos(models.Model):
    Periodo = models.CharField(max_length=32, null=False)
    MesInicio = models.IntegerField(choices=[(i, i) for i in range(1, 13)], null=False, default=1)
    MesFInal = models.IntegerField(choices=[(i, i) for i in range(1, 13)], null=False, default=12)

    def __str__(self):
        return self.Periodo


class Rentas(models.Model):
    Referencia = models.CharField(max_length=16, null=False, unique=True)
    Usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE, null=False)
    Casillero = models.ForeignKey(Casilleros, on_delete=models.CASCADE, null=False)
    Periodo = models.ForeignKey(Periodos, on_delete=models.CASCADE, null=False)
    Estado = models.ForeignKey(Estados, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return self.Referencia


class Pagos(models.Model):
    Total = models.FloatField(blank=True, null=True)
    FechaPago = models.DateTimeField(auto_now_add=True, auto_now=False)
    NumRenta = models.ForeignKey(Rentas, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return str(self.id)